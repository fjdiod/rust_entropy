use std::env;
use std::fs::File;
use std::io::Read;

fn entropy(arr: &[i32; 256]) -> f32 {
    let mut prob:f32;
    let len = arr.iter().fold(0, |a, &b| a + b) as f32;
    let mut entropy = 0.0;
    for i in arr.iter() {
        prob = (*i as f32)/len;
        if prob > 0.0 {
            entropy -= prob * prob.log2();
        }
    }
    return entropy;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut f = File::open(&args[1]).unwrap();
    let mut contents = String::new();
    let mut counts = [0i32; 256];
    
    for byte in f.bytes() {
        counts[byte.unwrap() as usize] += 1;
    }
    
    println!("{}", entropy(&counts));
}
